create mysql database bigbasket.

change database settings in bigbasket/settings.py file.

"python manage.py migrate" # to migrate database run this command.

"python manage.py runserver" # to run local server.

localhost:8000 for regular user.

localhost:8000/admin for admin access.

utils.py contains utility methods.

bigbasket/settings.py contains all settings related this project.

bigbasket/urls.py contains root urls.

adminn/views.py contains adminn apis.

inventory/models.py contains product model for product information.

inventory/urls.py contains read api.

registration/views.py contains signin, signup api.

registration/urls.py contains singin, signup api urls.

template dir contains all html files.

static dir contains all static files.