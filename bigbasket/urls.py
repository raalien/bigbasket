"""bigbasket URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from inventory import urls as inventory_urls
from registration import urls as registration_urls
from adminn import urls as adminn_urls
#from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^admin/', include(adminn_urls)),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^registration/', include(registration_urls)),
    url(r'^inventory/', include(inventory_urls)),
    url(r'',include(inventory_urls)),
]
