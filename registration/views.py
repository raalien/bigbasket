import json
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User

# Create your views here.

def signin(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        return HttpResponse(json.dumps({'login_succ': True}),
            content_type='application/json')
    else:
        return HttpResponse(json.dumps({'login_succ': False}),
            content_type='application/json')

def signout(request):
    logout(request)
    return HttpResponseRedirect('/')

def register(request):
    username = request.POST.get('username')
    email = request.POST.get('email')
    password = request.POST.get('password')
    try:
        user = User.objects.get(username=username)
        return HttpResponse(json.dumps({'registration_succ': False, 'msg': 'username already registered'}), 
            content_type='application/json')
    except:
        user = User.objects.create(username=username, email=email)
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
        login(request, user)
        return HttpResponse(json.dumps({'registration_succ': True}),
            content_type='application/json')