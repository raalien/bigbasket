from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'product/update$', views.update_product),
	url(r'product/delete$', views.delete_product),
	url(r'inventory/fetch$', views.fetch_inventory),
	url(r'product/add$', views.add_product),
	url(r'', views.home),
]