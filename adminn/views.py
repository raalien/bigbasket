import json, os, sys
from django.conf import settings
from django.shortcuts import render, render_to_response
from django.http import HttpResponse
from django.template import RequestContext
from django.contrib.auth.decorators import login_required, user_passes_test
from django.views.decorators.csrf import csrf_exempt
from django.template import loader, Context

@login_required
@user_passes_test(lambda u: u.is_superuser)
def home(request):
    from inventory.models import Product
    products =  Product.objects.filter(deleted=False).order_by('-uploaded_at')
    total_pages = products.count() / 10 + (1 if products.count()%10 else 0)
    products = [product.to_json() for product in products[:10]]
    return render_to_response('admin/base_site.html',
        {'username': request.user.username, 
        'products': products, 'total': total_pages}, context_instance=RequestContext(request))

@login_required
@user_passes_test(lambda u: u.is_superuser)
def add_product(request):
    from utils import gen_random_str
    from inventory.models import Product
    pic = request.FILES.get('file')
    title = request.POST.get('title')
    pics_dir = settings.STATIC_PATH+'/pics/'
    fname, ext = os.path.splitext(pic.name)
    filename = gen_random_str() + ext
    f = open(pics_dir+filename, 'w')
    f.write(pic.read())
    f.close()
    Product.objects.create(added_by=request.user, title=title, 
        picname=filename)
    return HttpResponse(json.dumps({'added': True}),
        content_type='application/json')

@login_required
@user_passes_test(lambda u: u.is_superuser)
def update_product(request):
    from utils import gen_random_str
    from inventory.models import Product
    pic = request.FILES.get('file')
    title = request.POST.get('title')
    filename = None
    if pic:
        pics_dir = settings.STATIC_PATH+'/pics/'
        fname, ext = os.path.splitext(pic.name)
        filename = gen_random_str() + ext
        f = open(pics_dir+filename, 'w')
        f.write(pic.read())
        f.close()
    try:
        product = Product.objects.get(id=request.POST.get('id'))
        if filename:
            product.picname = filename
        if title:
            product.title = title
        product.save();
    except:
        pass
    return HttpResponse(json.dumps({'updated': True}),
        content_type='application/json')

@login_required
@user_passes_test(lambda u: u.is_superuser)
def fetch_inventory(request):
    from inventory.models import Product
    try:
        page = int(request.POST.get('page'))
    except:
        page = 1
    products = Product.objects.filter(deleted=False).order_by('-uploaded_at')
    total_pages = products.count() / 10 + (1 if products.count()%10 else 0)
    products = [product.to_json() for product in products[(page-1)*10:page*10]]
    template = loader.get_template('admin/inventory-table.html')
    html = template.render(Context({'products': products}))
    return HttpResponse(json.dumps({
        'html': html, 'total': total_pages}),
        content_type='application/json')

@login_required
@user_passes_test(lambda u: u.is_superuser)
def delete_product(request):
    product_id = request.POST.get('id')
    from inventory.models import Product
    try:
        product = Product.objects.get(id=product_id)
        product.deleted = True
        product.save()
        products = Product.objects.filter(deleted=False)
        total_pages = products.count() / 10 + (1 if products.count()%10 else 0)
        return HttpResponse(json.dumps({'deleted': True, 'total': total_pages}),
            content_type='application/json')
    except Exception, e:
        print e
        return HttpResponse(json.dumps({'deleted': False}),
            content_type='application/json')