import string, json, random

def gen_random_str():
	return ''.join(map(lambda a, b, c: a+b+c, 
		random.sample(string.letters, 5), 
		random.sample(string.digits, 5), 
		random.sample(string.letters, 5)))