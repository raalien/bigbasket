$(function() {

    $('#login-form-link').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});

	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});

    $("#login-submit").click(function(e) {
        data = {};
        data["username"] = $("#login-form").find("#username").val();
        data["password"] = $("#login-form").find("#password").val();
        data['_csrf'] = $("input[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: "POST",
            url: "/registration/signin",
            data: data,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRFToken", $("input[name=csrfmiddlewaretoken]").val());
            },
            success: function(resp) {
                window.location.href=window.location.search.substr(window.location.search.indexOf('/'))
            },
            error: function(error) {
                console.log(error);
            }
        });
        e.preventDefault();
    });

    $("#register-submit").click(function(e) {
        data = {};
        data["username"] = $("#register-form").find("#username").val();
        data["email"] = $("#register-form").find("#email").val();
        data["password"] = $("#register-form").find("#password").val();
        data['_csrf'] = $("input[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: "POST",
            url: "/registration/register",
            data: data,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRFToken", $("input[name=csrfmiddlewaretoken]").val());
            },
            success: function(resp) {
                if (resp["registration_succ"] == true) {
                    window.location.search.substr(window.location.search.indexOf('/'))
                } else {

                }
            },
            error: function(error) {
                console.log(error);
            }
        });
        e.preventDefault();
    });
});