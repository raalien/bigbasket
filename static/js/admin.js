$(function() {
    var fetch_inventory = function(page) {
        data = {};
        data['page'] = page;
        $.ajax({
            method: "POST",
            url: "/admin/inventory/fetch",
            data: data,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRFToken", $("input[name=csrfmiddlewaretoken]").val());
            },
            success: function(resp) {
                $(".inventory-display-body").html(resp.html);
                $("#pager").attr("data-total-pages", resp.total);
            },
            error: function(error) {
                console.log(error);
            }
        });
    };

    $("#pager").twbsPagination({
        totalPages: $("#pager").attr("data-total-pages"),
        visiblePages: 10,
        onPageClick: function (event, page) {
            fetch_inventory(page);
        }
    });
    var update_pager = function(total) {
        if (total == undefined) {
            total = $("#pager").attr("data-total-pages");
        }
        var curpage = $("#pager").twbsPagination("getCurrentPage");
        $("#pager").twbsPagination('destroy');
        $("#pager").twbsPagination($.extend({}, {}, {currentPage:curpage, totalPages:total}));
    }
    $("#add-product-btn").click(function(e) {
        inputfile = $("#product-pic").val();
        if (inputfile) {
            var data = new FormData();
            data.append("title", $("#product-title").val());
            data.append("file", $("#product-pic")[0].files[0]);
            data.append("_csrf", $("input[name=csrfmiddlewaretoken]").val());
            $.ajax({
                method: "POST",
                url: "/admin/product/add",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("X-CSRFToken", $("input[name=csrfmiddlewaretoken]").val());
                    $("#add-product-spinner").addClass("spinner");
                },
                success: function(resp) {
                    $("#add-product-spinner").removeClass("spinner");
                    fetch_inventory($("#pager").twbsPagination("getCurrentPage"));
                    update_pager($("#pager").attr("data-total-pages"));
                },
                error: function(error) {
                    console.log(error);
                    $("#add-product-spinner").removeClass("spinner");
                }
            });
        } else {
            alert("select ");
        }
        e.preventDefault();
    });

    $(document).on("click", ".edit-product", function() {
        $(this).attr("disabled", "true");
        $(this).parent().parent().find('.delete-product').attr("disabled", "true");
        tr_clone = $(".edit-operation").find("tr").clone();
        tr_index = $(this).parent().parent().index();
        $(this).parent().parent().addClass("in-process");
        tr_clone.addClass('in-process');
        $(document).find(".inventory-display-body table > tbody > tr").eq(tr_index).after(tr_clone);
    });

    $(document).on("click", ".close-update", function() {
        tr = $(this).parent().parent();
        $(document).find(".inventory-display-body table > tbody > tr").eq(tr.index()-1).removeClass("in-process");
        $(document).find(".inventory-display-body table > tbody > tr").eq(tr.index()-1).find(".edit-product").removeAttr("disabled");
        $(document).find(".inventory-display-body table > tbody > tr").eq(tr.index()-1).find(".delete-product").removeAttr("disabled");
        $(this).parent().parent().remove();
    });

    $(document).on("click", ".delete-product", function() {
        data = {};
        var this_el = $(this);
        data["id"] = this_el.parent().parent().find(".product-id").text();
        $.ajax({
            method: "POST",
            url: "/admin/product/delete",
            data: data,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRFToken", $("input[name=csrfmiddlewaretoken]").val());
            },
            success: function(resp) {
                if (resp.deleted){
                    fetch_inventory($("#pager").twbsPagination("getCurrentPage"));
                    update_pager();
                } else {
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
    $(document).on("click", ".update-product", function() {
        var data = new FormData();
        el_index = $(this).parent().parent().index();
        data.append("id", $(document).find(".inventory-display-body table > tbody > tr").eq(el_index-1).find(".product-id").text());
        inputfile = $("#product-new-pic").val();
        if (inputfile) {
            data.append("file", $("#product-new-pic")[0].files[0]);
        } else if ($("#product-new-title").val()) {
            data.append("title", $("#product-new-title").val());
        } else {
            return;
        }
        $.ajax({
            method: "POST",
            url: "/admin/product/update",
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRFToken", $("input[name=csrfmiddlewaretoken]").val());
            },
            success: function(resp) {
                fetch_inventory($("#pager").twbsPagination("getCurrentPage"));
                update_pager();
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});