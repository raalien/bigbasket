$(function() {
    $("#pager").twbsPagination({
        totalPages: $("#pager").attr("data-total-pages"),
        visiblePages: 10,
        onPageClick: function (event, page) {
            fetch_inventory(page);
        }
    });

    var fetch_inventory = function(page) {
        data = {};
        data['page'] = page;
        $.ajax({
            method: "POST",
            url: "/inventory/fetch",
            data: data,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRFToken", $("input[name=csrfmiddlewaretoken]").val());
            },
            success: function(resp) {
                $(".inventory-display-body").html(resp.html);
            },
            error: function(error) {
                console.log(error);
            }
        });
    };
});