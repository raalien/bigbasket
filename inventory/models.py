from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Product(models.Model):
    added_by = models.ForeignKey(User)
    title = models.CharField(max_length=50)
    picname = models.CharField(max_length=200)
    available = models.BooleanField(default=False)
    uploaded_at = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False)

    def to_json(self):
        return {
            'id': self.id,
            'add_by': self.added_by.username,
            'title': self.title,
            'pic_url': 'http://localhost:8000/static/pics/'+self.picname,
            'available': self.available
        }