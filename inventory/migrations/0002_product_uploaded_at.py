# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='uploaded_at',
            field=models.DateTimeField(default=datetime.datetime(2017, 3, 2, 9, 32, 36, 234118, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
