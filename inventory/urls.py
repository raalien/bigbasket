from django.conf.urls import url
from . import views 

urlpatterns = [
    url(r'fetch$', views.fetch_inventory),
    url(r'home$', views.home),
    url(r'', views.home)
]