import os, sys, json
from django.conf import settings
from django.shortcuts import render
from django.contrib.auth.decorators import login_required, user_passes_test
from django.template import RequestContext, loader, Context
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse

# Create your views here.

@login_required
def home(request):
    from inventory.models import Product
    products =  Product.objects.filter(deleted=False).order_by('-uploaded_at')
    total_pages = products.count() / 10 + (1 if products.count()%10 else 0)
    products = [product.to_json() for product in products[:10]]
    return render_to_response('inventory/home.html',
        {'username': request.user.username, 
        'products': products, 'total': total_pages}, context_instance=RequestContext(request))

@login_required
def fetch_inventory(request):
    from inventory.models import Product
    try:
        page = int(request.POST.get('page'))
    except:
        page = 1
    products = Product.objects.filter(deleted=False).order_by('-uploaded_at')
    total_pages = products.count() / 10 + (1 if products.count()%10 else 0)
    products = [product.to_json() for product in products[(page-1)*10:page*10]]
    template = loader.get_template('inventory/inventory-table.html')
    html = template.render(Context({'products': products, 'total': (page-1)*10}))
    return HttpResponse(json.dumps({
        'html': html, 'total': total_pages}),
        content_type='application/json')